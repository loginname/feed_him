﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class loonGenerator : MonoBehaviour {
    public GameObject bonusLoon; // prefab of loon will spawn
    public static GameObject[] setLoons = new GameObject[3]; // 3 loons will set to bonus loon
    public static int countBurstLoons = 0; // how many loons is burst
    private float minDelay = 10; // min time before spawn
    private float maxDelay = 30; // max time before spawn
    void Update() {
        if(currentGameSituation.gameOver == false) {
            transform.Translate(Vector2.up * Time.deltaTime * boxoonController.idleSpeed); // move
        }

        if(countBurstLoons != 0) { // if any loon is burst
            for(int i = 0; i < countBurstLoons; i++) {
                countBurstLoons--;
                Invoke("Spawn", Random.Range(minDelay, maxDelay)); // spawn bonus loon in time
            }
        }

        // if((setLoons[0].CompareTag("burst") || setLoons[1].CompareTag("burst") || setLoons[2].CompareTag("burst"))) {
        //     if(emptyLoon == true && currentGameSituation.transition == false && currentGameSituation.gameOver == false) {
        //         emptyLoon = false;
        //         //Invoke("Spawn", Random.Range(minDelay, maxDelay)); // spawn in time
        //         StartCoroutine(checkForBurst());
        //     } 
        // }
    }
    public void Spawn() { // spawn bonus loon
        Instantiate(bonusLoon, new Vector2(Random.Range(-3, 3), transform.position.y), Quaternion.identity);
    }
    
    // private IEnumerator checkForBurst() {
    //     Instantiate(bonusLoon, new Vector2(Random.Range(-3, 3), transform.position.y), Quaternion.identity); 
    //     yield return new WaitForSeconds(30); // Random.Range(minDelay, maxDelay)
    //     emptyLoon = true;
    // }
}
