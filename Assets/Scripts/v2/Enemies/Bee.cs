﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bee : MonoBehaviour {
    public float moveSpeed = 1.5f;
    private float positionY; // start Y position
    public Vector2 moveSide; // move to right or left
    public Vector2 moveHeight; // move to up or down
    void Start() {
        GetComponent<AudioSource>().Play();
        positionY += gameObject.GetComponent<Transform>().position.y; // set start Y position
    }
    void Update() {
        gameObject.transform.Translate((moveSide + moveHeight) * moveSpeed * Time.deltaTime); // Move
        if (gameObject.GetComponent<Transform>().position.y >= positionY + .5) { // change vertical to Down
            //moveSide -= Vector2.up;
            moveHeight = Vector2.down;
        } else if (gameObject.GetComponent<Transform>().position.y <= positionY - .5) { // change vertical to Up
            //moveSide -= Vector2.down;
            moveHeight = Vector2.up;
        }

        if (gameObject.GetComponent<Transform>().position.x <= -3) { // change horizontal to Right
            //moveSide -= Vector2.left;
            moveSide = Vector2.right;
            gameObject.GetComponent<SpriteRenderer>().flipX = false; // change direction of image
        } else if (gameObject.GetComponent<Transform>().position.x >= 3) { // change horizontal to Left
            //moveSide -= Vector2.right;
            moveSide = Vector2.left;
            gameObject.GetComponent<SpriteRenderer>().flipX = true; // change direction of image
        }
    }
}
