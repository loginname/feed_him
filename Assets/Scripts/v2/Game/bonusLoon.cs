﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bonusLoon : MonoBehaviour {
    public GameObject[] set_balloons = new GameObject[Boxoon.count_balloons];
    public int rotate_speed;
    public int side;
    void Start() {
        for(int i = 0; i < Boxoon.count_balloons; i++) {
            set_balloons[i] = loonGenerator.set_balloons[i];
        }
    }
    void Update() {
        transform.Rotate(0, 0, Time.deltaTime * rotate_speed * side);

        if(transform.eulerAngles.z <= 355 && transform.eulerAngles.z > 353) {
            side = 1;
        } else if (transform.eulerAngles.z > 5 && transform.eulerAngles.z < 7) {
            side = -1;
        }
    }
    void OnTriggerEnter2D(Collider2D loon) {
        for(int i = 0; i < Boxoon.count_balloons; i++) {
            if(set_balloons[i].CompareTag("burst")) {
                set_balloons[i].GetComponent<SpriteRenderer>().enabled = true;
                set_balloons[i].GetComponent<CircleCollider2D>().enabled = true;
                set_balloons[i].GetComponent<HingeJoint2D>().enabled = true;
                set_balloons[i].tag = "loon";

                Boxoon.idle_speed += Boxoon.up_speed_start / 3;
                Boxoon.move_speed_vertical += Boxoon.move_speed_vertical_start / 3;
                Boxoon.move_speed_horizontal += Boxoon.move_speed_horizontal_start / 3;

                Destroy(gameObject); 

                break;
            }
        }
    }
}
