﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class addBalloon : MonoBehaviour
{
    public GameObject loon;
    public GameObject[] set_balloons = new GameObject[balloons.count_balloons];
    public int rotate_speed;
    public int side;
    public bool empty_loon;
    void Update() 
    {
        loon.transform.Rotate(0, 0, Time.deltaTime * rotate_speed * side);

        if(loon.transform.eulerAngles.z <= 355 && loon.transform.eulerAngles.z > 353) {
            side = 1;
        } else if (loon.transform.eulerAngles.z > 5 && loon.transform.eulerAngles.z < 7) {
            side = -1;
        }
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        for(int i = 0; i < balloons.count_balloons; i++) {
            if(set_balloons[i].CompareTag("burst")) {
                set_balloons[i].GetComponent<SpriteRenderer>().enabled = true;
                set_balloons[i].GetComponent<CircleCollider2D>().enabled = true;
                set_balloons[i].GetComponent<HingeJoint2D>().enabled = true;
                set_balloons[i].tag = "loon";

                balloons.up_speed += balloons.up_speed_start / 3;
                balloons.move_speed_vertical += balloons.move_speed_vertical_start / 3;

                Destroy(loon); 

                break;
            }
        }
    }
}
