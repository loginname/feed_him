﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mosquito : MonoBehaviour
{
    public GameObject insect;
    public GameObject add_loon;
    public float moveSpeed;
    public Vector2 moveSide;
    public Vector2 moveHeight;
    void Start()
    {
        moveSide = Vector2.right;
        moveHeight = Vector2.up;
    }
    void Update()
    {
        insect.transform.Translate((moveSide + moveHeight) * moveSpeed * Time.deltaTime);

        if (insect.GetComponent<Transform>().position.y >= 2.2) {
            //moveSide -= Vector2.up;
            moveHeight = Vector2.down;
        } else if (insect.GetComponent<Transform>().position.y <= 1.8) {
            //moveSide -= Vector2.down;
            moveHeight = Vector2.up;
        }

        if (insect.GetComponent<Transform>().position.x <= -2) {
            //moveSide -= Vector2.left;
            moveSide = Vector2.right;
            insect.GetComponent<SpriteRenderer>().flipX = false;
        } else if (insect.GetComponent<Transform>().position.x >= 2) {
            //moveSide -= Vector2.right;
            moveSide = Vector2.left;
            insect.GetComponent<SpriteRenderer>().flipX = true;
        }
            
    }
    void OnTriggerEnter2D(Collider2D balloon)
    {
        if (balloon.CompareTag("loon")) // if mosquito touch a loon then disabled loon's parametres
        {
            balloon.GetComponent<SpriteRenderer>().enabled = false;
            balloon.GetComponent<CircleCollider2D>().enabled = false;
            balloon.GetComponent<HingeJoint2D>().enabled = false;

            balloon.tag = "burst";

            balloons.up_speed -= balloons.up_speed_start / 3;
            balloons.move_speed_vertical -= balloons.move_speed_vertical_start / 3;

            add_loon.SetActive(true);

            //Destroy(other.gameObject);

            //balloons.count_balloons -= 1;
        
         } /* else { // if mosquito touch the box?? then box destroy
            Destroy(other.gameObject);
            CakieController.objEx = false;
        } */
    }
}
