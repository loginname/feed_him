﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cactus : MonoBehaviour
{
    public GameObject add_loon;
    // public float move_speed;
    void Update()
    {
        
    }

    void OnTriggerEnter2D(Collider2D balloon)
    {
        if (balloon.CompareTag("loon")) // if mosquito touch a loon then disabled loon's parametres
        {
            balloon.GetComponent<SpriteRenderer>().enabled = false;
            balloon.GetComponent<CircleCollider2D>().enabled = false;
            balloon.GetComponent<HingeJoint2D>().enabled = false;

            balloon.tag = "burst";

            balloons.up_speed -= balloons.up_speed_start / 3;
            balloons.move_speed_vertical -= balloons.move_speed_vertical_start / 3;

            add_loon.SetActive(true);

            //Destroy(other.gameObject);

            //balloons.count_balloons -= 1;
        
         } /* else { // if mosquito touch the box?? then box destroy
            Destroy(other.gameObject);
            CakieController.objEx = false;
        } */
    }
}
