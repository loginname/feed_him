﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChunksGenerator : MonoBehaviour {
    public Transform Camera;
    public Chunk[] ChunksPrefab;
    public Chunk FirstChunk;
    private List<Chunk> spawnedChunks = new List<Chunk>();
    void Start() {
        spawnedChunks.Add(FirstChunk);
    }
    void Update() {
        // Debug.Log("Camera position: " + Camera.position.y);
        // Debug.Log("Chunk end position: " + spawnedChunks[spawnedChunks.Count-1].End.position.y);
        if(Camera.position.y > spawnedChunks[spawnedChunks.Count-1].End.position.y - 7) {
            SpawnChunk();
        }
    }
    void SpawnChunk() {
        Chunk newChunk = Instantiate(ChunksPrefab[Random.Range(0, ChunksPrefab.Length)]);
        newChunk.transform.position = spawnedChunks[spawnedChunks.Count-1].End.position - newChunk.Begin.localPosition;
        spawnedChunks.Add(newChunk);

        if(spawnedChunks.Count >= 3) {
            Destroy(spawnedChunks[0].gameObject);
            spawnedChunks.RemoveAt(0);
        }
    }
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Chunk GetRandomChunk() { 
    //     List<float> chances = new List<float>();
    //     for(int i = 0; i < ChunksPrefab.Length; i++) {
    //         chances.Add(ChunksPrefab[i].ChanceFromDistance.Evaluate(Camera.transform.position.y));
    //     }

    //     float value = Random.Range(0, chances.Sum());
    //     float sum = 0;

    //     for(int i = 0; i < chances.Count; i++) {
    //         sum += chances[i];
    //         if(value < sum) {
    //             return ChunksPrefab[i];
    //         } 
    //     }

    //     return ChunksPrefab[ChunksPrefab.Length-1];
    // }
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
