﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGGenerator : MonoBehaviour {
    public Transform Camera;
    public backGround bgSprite;
    public backGround firstBG;
    private List<backGround> spawnedBG = new List<backGround>();
    void Start() {
        spawnedBG.Add(firstBG);
    }
    void Update() {
        // Debug.Log("Camera position: " + Camera.position.y);
        // Debug.Log("Chunk end position: " + spawnedBG[spawnedBG.Count-1].End.position.y);
        if(Camera.position.y > spawnedBG[spawnedBG.Count-1].End.position.y - 7) {
            SpawnBG();
        }
    }
    void SpawnBG() {
        backGround newBG = Instantiate(bgSprite);
        newBG.transform.position = spawnedBG[spawnedBG.Count-1].End.position - newBG.Begin.localPosition;
        spawnedBG.Add(newBG);

        if(spawnedBG.Count >= 3) {
            //Destroy(spawnedBG[0].gameObject);
            spawnedBG.RemoveAt(0);
        }
    }
}
