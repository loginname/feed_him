﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class loonGenerator : MonoBehaviour {
    public GameObject bonusLoon;
    public static GameObject[] set_balloons = new GameObject[Boxoon.count_balloons];
    public static bool empty_loon = false;
    public float MinDelay;
    public float MaxDelay;
    void Update() {
        transform.Translate(Vector2.up * Time.deltaTime * Boxoon.idle_speed);

        if(empty_loon) {
            empty_loon = false;
            Invoke("Spawn", Random.Range(MinDelay, MaxDelay));
        } 

        //for(int i = 0; i < Boxoon.count_balloons; i++) {
            //if(set_balloons[i].CompareTag("burst")) {
                //Invoke("Spawn", Random.Range(MinDelay, MaxDelay));
                //break;
            //}
        //}
    }
    public void Spawn() {
        Instantiate(bonusLoon, new Vector2(Random.Range(-3, 3), transform.position.y), Quaternion.identity);
    }
}
