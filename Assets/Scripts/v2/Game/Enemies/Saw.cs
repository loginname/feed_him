﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Saw : MonoBehaviour {
    public float moveSpeed;
    ////////////////////////////////////////////////////////
    public bool isMoving = true;
    public float max_x = 3;
    public float min_x = -3;
    public bool changeDirection = false;
    public Vector2 moveDirection;
    ////////////////////////////////////////////////////////
    public bool isCircle = false;
    public float radius = 1;
    public float angle = 0;
    ////////////////////////////////////////////////////////
    void Update() {
        if(isMoving) {
            gameObject.transform.Translate(moveDirection * moveSpeed * Time.deltaTime);

            if((gameObject.GetComponent<Transform>().position.x <= min_x && changeDirection == false) 
            || (gameObject.GetComponent<Transform>().position.x >= max_x && changeDirection == false)) {
                moveDirection *= -1;
                changeDirection = true;
            } else if (gameObject.GetComponent<Transform>().position.x > min_x 
                    && gameObject.GetComponent<Transform>().position.x < max_x 
                    && changeDirection == true) {
                        changeDirection = false;
                    }
        }

        if(isCircle) {
            angle += Time.deltaTime; // smooth change angle

            var x = Mathf.Cos(angle * moveSpeed) * radius;
            var y = Mathf.Sin(angle * moveSpeed) * radius;
            transform.position = new Vector2(x, y);
        }
    }
}

