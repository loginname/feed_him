﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class touchLoon : MonoBehaviour {
    //public GameObject add_loon;
    void OnTriggerEnter2D(Collider2D loon) {
        if (loon.CompareTag("loon")) {
            loon.GetComponent<SpriteRenderer>().enabled = false;
            loon.GetComponent<CircleCollider2D>().enabled = false;
            loon.GetComponent<HingeJoint2D>().enabled = false;

            loon.tag = "burst";
            loonGenerator.empty_loon = true;

            Boxoon.idle_speed -= Boxoon.up_speed_start / 3;
            Boxoon.move_speed_vertical -= Boxoon.move_speed_vertical_start / 3;
            Boxoon.move_speed_horizontal -= Boxoon.move_speed_horizontal_start / 3;

            //add_loon.SetActive(true);
        }
    }
}
