﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class CactusSold : MonoBehaviour {
    public GameObject needle;
    public Transform spawn_position;
    public int angle = 0;
    public float x_position = -0.7f;
    public float y_position = 0.3f;
    public void spawnNeedle() {
        Instantiate(needle, new Vector2(spawn_position.transform.position.x, spawn_position.transform.position.y), Quaternion.Euler(0f, 0f, angle));
    }
}
