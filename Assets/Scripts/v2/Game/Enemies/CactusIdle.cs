﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CactusIdle : MonoBehaviour
{
    public GameObject add_loon;
    void OnTriggerEnter2D(Collider2D balloon)
    {
        if (balloon.CompareTag("loon"))
        {
            balloon.GetComponent<SpriteRenderer>().enabled = false;
            balloon.GetComponent<CircleCollider2D>().enabled = false;
            balloon.GetComponent<HingeJoint2D>().enabled = false;

            balloon.tag = "burst";

            balloons.up_speed -= balloons.up_speed_start / 3;
            balloons.move_speed_vertical -= balloons.move_speed_vertical_start / 3;
            balloons.move_speed_horizontal -= balloons.move_speed_horizontal_start / 3;

            add_loon.SetActive(true);
         }
    }
}
