﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Needle : MonoBehaviour {
    public float moveSpeed = 3;
    public Vector2 direction;
    void Update() {
        gameObject.transform.Translate(direction * moveSpeed * Time.deltaTime);
        
        Destroy(gameObject, 3);
    }
}
