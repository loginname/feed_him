﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bee : MonoBehaviour {
    public float moveSpeed;
    public float position_y;
    public Vector2 moveSide;
    public Vector2 moveHeight;
    void Start() {
        position_y += gameObject.GetComponent<Transform>().position.y;
    }
    void Update() {
        gameObject.transform.Translate((moveSide + moveHeight) * moveSpeed * Time.deltaTime);
        if (gameObject.GetComponent<Transform>().position.y >= position_y + .5) {
            //moveSide -= Vector2.up;
            moveHeight = Vector2.down;
        } else if (gameObject.GetComponent<Transform>().position.y <= position_y - .5) {
            //moveSide -= Vector2.down;
            moveHeight = Vector2.up;
        }

        if (gameObject.GetComponent<Transform>().position.x <= -3) {
            //moveSide -= Vector2.left;
            moveSide = Vector2.right;
            gameObject.GetComponent<SpriteRenderer>().flipX = false;
        } else if (gameObject.GetComponent<Transform>().position.x >= 3) {
            //moveSide -= Vector2.right;
            moveSide = Vector2.left;
            gameObject.GetComponent<SpriteRenderer>().flipX = true;
        }
    }
}
