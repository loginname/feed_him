﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boxoon : MonoBehaviour {
    public GameObject boxoon;
    public GameObject box;
    public GameObject camera_main;
    public static int count_balloons = 3;
    public GameObject[] set_balloons = new GameObject[count_balloons];
    public Transform[] balloons_rotations;
    public Transform box_rotation;
    public static Transform boxoon_position;
    public static float up_speed_start = 1;
    public static float idle_speed = 1;
    public static float gg_speed = 3;
    public static float move_speed_horizontal_start = 3;
    public static float move_speed_horizontal = 3;
    public static float move_speed_vertical_start = 2;
    public static float move_speed_vertical = 2;
    public float rotate_speed;
    public float return_speed;
    // void Start() {
        // for(int i = 0; i < rotationBall.Length; i++) {
        //     Debug.Log(balloonS[i].GetComponent<Transform>().rotation);
        //     rotationBall[i] = balloonS[i].GetComponent<Transform>();
        // }
        // rotationBall[0] = balloonS[0].transform.localEulerAngles.z;
        // Debug.Log("1 : " + rotationBall[0]);
        // rotationBall[1] = balloonS[1].transform.localEulerAngles.z;
        // Debug.Log("2 : " + rotationBall[1]);
        // rotationBall[2] = balloonS[2].transform.localEulerAngles.z;
        // Debug.Log("3 : " + rotationBall[2]);
    // }
    void Start() {
        for(int i = 0; i < count_balloons; i++) {
            loonGenerator.set_balloons[i] = set_balloons[i];
        }
    }
    void Update() {
        boxoon_position = boxoon.GetComponent<Transform>();
        // for(int i = 0; i < count_balloons; i++) {
        //     //set_balloons[i] = GameObject.FindGameObjectWithTag("loon");
        //     set_balloons[i] = GameObject.Find("balloons1_" + i);
        //     //Debug.Log(set_balloons[i]);
        // }

        // for(int i = 0; i < count_balloons; i++) {
        //     if(set_balloons[i].CompareTag("burst")) {
        //         loonGenerator.empty_loon = true;
        //     }
        // }

        if (set_balloons[0].CompareTag("loon") || set_balloons[1].CompareTag("loon") || set_balloons[2].CompareTag("loon")) {
            boxoon.transform.Translate(Vector2.up * Time.deltaTime * idle_speed);
            //Debug.Log(count_balloons);

            if(Input.GetKey(KeyCode.A) && boxoon.GetComponent<Transform>().position.x > -2.5) {
                boxoon.transform.Translate(Vector2.left * Time.deltaTime * move_speed_horizontal);
                if(set_balloons[0].transform.localEulerAngles.z < 70) {
                    for(int i = 0; i < set_balloons.Length; i++) {
                        set_balloons[i].transform.Rotate(0, 0, Time.deltaTime * rotate_speed);
                    }
                    box.transform.Rotate(0, 0, Time.deltaTime * rotate_speed);
                }
                // if (mainCam.GetComponent<Transform>().position.x >= -0.7) {
                //     mainCam.transform.Translate(Vector2.left * Time.deltaTime * speed);
                // }
                // if(balloonS[2].transform.localEulerAngles.z >= 290) { }
            } else if (Input.GetKey(KeyCode.D) && boxoon.GetComponent<Transform>().position.x < 2.5) {
                boxoon.transform.Translate(Vector2.right * Time.deltaTime * move_speed_horizontal);
                if(set_balloons[2].transform.localEulerAngles.z > 290) {
                    for(int i = 0; i < set_balloons.Length; i++) {
                        set_balloons[i].transform.Rotate(0, 0, -Time.deltaTime * rotate_speed);
                    }   
                    box.transform.Rotate(0, 0, -Time.deltaTime * rotate_speed);
                }
                
                // if (mainCam.GetComponent<Transform>().position.x <= 0.7) {
                //     mainCam.transform.Translate(Vector2.right * Time.deltaTime * speed);
                // }
            } else if (Input.GetKey(KeyCode.W) && boxoon.GetComponent<Transform>().position.y < camera_main.GetComponent<Transform>().position.y + 3.5) {
                boxoon.transform.Translate(Vector2.up * Time.deltaTime * move_speed_horizontal * move_speed_vertical);

            } else if (Input.GetKey(KeyCode.S) && boxoon.GetComponent<Transform>().position.y > camera_main.GetComponent<Transform>().position.y - 5.5) {
                boxoon.transform.Translate(Vector2.down * Time.deltaTime * move_speed_horizontal * move_speed_vertical);

            } else { // return balloons on start positions
                for(int i = 0; i < set_balloons.Length; i++) {
                    if(set_balloons[i].transform != balloons_rotations[i]) {
                        set_balloons[i].transform.rotation = Quaternion.Slerp(set_balloons[i].transform.rotation, balloons_rotations[i].rotation, return_speed * Time.deltaTime);
                    }
                }
                //Debug.Log("Hey, here is box position!");
                box.transform.rotation = Quaternion.Slerp(box.transform.rotation, box_rotation.rotation, return_speed * Time.deltaTime);
            }
            
            // if(Input.GetKeyUp(KeyCode.A)) {
            //     for(int i = 0; i < balloonS.Length; i++) {
            //         balloonS[i].transform.Rotate(0, 0, Time.deltaTime * speedRotate);
            //     }
            // }
        } else {
            boxoon.transform.Translate(Vector2.down * Time.deltaTime * gg_speed);
            Destroy(gameObject, 3);
        }
        
    }
}
