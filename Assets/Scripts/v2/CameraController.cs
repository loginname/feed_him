﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
    public GameObject Character; // Cackie
    void Update() {
        if(currentGameSituation.endlessMode == true) { // Endless Mode
            //Debug.Log(currentGameSituation.gameOver);
            if(currentGameSituation.gameOver == false) { // if game isnt over
                //Debug.Log("wow, hi, im a bag :В");
                transform.Translate(Vector2.up * Time.deltaTime * boxoonController.idleSpeed);
            } else {
                //Debug.Log("wow, hi, im a bag :В");
                transform.Translate(Vector2.zero);
            }
        } else if(currentGameSituation.endlessMode == false) { // Story Mode
            if(currentGameSituation.finish == false && currentGameSituation.gameOver == false) // if game isnt over
                if(transform.position.y <= Character.transform.position.y - 5) // if position of camera <= position of cackie - 5
                    transform.Translate(Vector2.up * Time.deltaTime * boxoonController.idleSpeed); // go up
                else transform.Translate(Vector2.zero); // stay

            // else if(currentGameSituation.finish == true) // if player has gone to finish
            //     if(transform.position.y <= Character.transform.position.y + 5) // if camera <= position of cackie + 5
            //         transform.Translate(Vector2.up * Time.deltaTime * boxoonController.idleSpeed); // go up

            else if(currentGameSituation.gameOver == true) // if player lost
                transform.Translate(Vector2.zero); // stay
        }
    }
}