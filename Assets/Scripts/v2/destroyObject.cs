﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class destroyObject : MonoBehaviour {
    public int timeBeforeDestroy = 0;
    void Start() {
        Destroy(gameObject, timeBeforeDestroy);
    }
}
