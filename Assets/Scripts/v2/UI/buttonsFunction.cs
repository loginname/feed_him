﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using GoogleMobileAds.Api;

public class buttonsFunction : MonoBehaviour {
    public GameObject mainMenu; // panel in Menu
    public GameObject chooseMode; // panel in Menu
    public GameObject scoreBoard; // panel in Menu
    public GameObject storyMode; // panel in Menu
    public GameObject miniMenu; // panel in game
    public GameObject gameMenu; // UI in game
    public GameObject diaryMenu; // diary panel un game
    public GameObject finishStoryMenu; // panel in game
    public GameObject finishEndlessMenu; // panel in game
    public GameObject gameOverMenu; // panel in game
    public GameObject settings; // panel in Menu
    public GameObject info; // panel in Menu
    public GameObject exit; // panel in Menu
    public GameObject onVolumeButton; // button volume
    public GameObject offVolumeButton; // button volume
    public GameObject[] finishLoons = new GameObject[3]; // final score's loons (story mode)
    public Sprite loonIconOn; // final score's ON loons
    public AudioClip pressButton;
    public AudioClip closeBook;

    private int countGameEnd = 0;
    private InterstitialAd ad;
    private const string banner = "ca-app-pub-3535789668157108/7456081673";
    private const string gameEndAD = "ca-app-pub-3535789668157108/5717841631";
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    void Start() {
        MobileAds.Initialize(initStatus => { });
        GetComponent<AudioSource>().Play();

        if(currentGameSituation.firstLaunch == false && currentGameSituation.inGame == false) {
            if(currentGameSituation.endlessMode == true) {
                chooseMode.SetActive(true);
                mainMenu.SetActive(false);
            } else {
                storyMode.SetActive(true);
                mainMenu.SetActive(false);
            }
        }

        currentGameSituation.firstLaunch = false;
    }
    void Update() {
        if(currentGameSituation.endlessMode == true) { // if endless mode
            if(currentGameSituation.gameOver == true && currentGameSituation.transition == false) { // if all loons are burst
                finishEndless();
            }
        } else if(currentGameSituation.endlessMode == false) { // if story mode
            if(currentGameSituation.finish == true && currentGameSituation.transition == false) { // if player has gone to Cackie
                finishLevel();
            } else if(currentGameSituation.gameOver == true && currentGameSituation.transition == false) { // if all loons are burst
                GameOver();
            }
        }
    }
    public void ChooseMode() { // to choose mode from main menu
        GetComponent<AudioSource>().PlayOneShot(pressButton);
        mainMenu.SetActive(false);
        chooseMode.SetActive(true);
    }
    public void ScoreBoard() { // to score board from choose mode
        GetComponent<AudioSource>().PlayOneShot(pressButton);
        scoreBoard.SetActive(true);
        chooseMode.SetActive(false);
    }
    public void BackFromScoreBoard() { // to choose mode from score mode
        GetComponent<AudioSource>().PlayOneShot(pressButton);
        scoreBoard.SetActive(false);
        chooseMode.SetActive(true);
    }
    public void LoadLevel(int sceneNumb) { // load scene
        BannerView bannerV = new BannerView(banner, AdSize.Banner, AdPosition.Bottom);
        AdRequest request = new AdRequest.Builder().Build();
        bannerV.LoadAd(request);

        currentGameSituation.inGame = true;

        GetComponent<AudioSource>().PlayOneShot(pressButton);
        currentGameSituation.activeScene = sceneNumb;

        currentGameSituation.Restart();
        SceneManager.LoadScene(sceneNumb);
    }
    public void ChooseLevel() { // to story mode from choose mode
        GetComponent<AudioSource>().PlayOneShot(pressButton);
        chooseMode.SetActive(false);
        storyMode.SetActive(true);
    }
    public void BackFromLevels() { // to choose mode from story mode
        GetComponent<AudioSource>().PlayOneShot(pressButton);
        storyMode.SetActive(false);
        chooseMode.SetActive(true);
    }
    public void BackFromChooseMode() { // to main menu from choose mode
        GetComponent<AudioSource>().PlayOneShot(pressButton);
        chooseMode.SetActive(false);
        mainMenu.SetActive(true);
    }
//////////////////////////////////////////////////////////
    public void CloseDiaryMenu() {
        GetComponent<AudioSource>().PlayOneShot(closeBook, 10F);
        diaryMenu.SetActive(false);
        gameMenu.SetActive(true);

        Time.timeScale = 1;
    }
    public void Pause() { // to pause menu
        GetComponent<AudioSource>().PlayOneShot(pressButton);
        // Debug.Log(currentGameSituation.transition);
        currentGameSituation.transition = true;
        // Debug.Log(currentGameSituation.transition);
        gameMenu.SetActive(false);
        miniMenu.SetActive(true);
        Time.timeScale = 0; // game paused
    }
    public void toMainMenu() { // to main menu from game's scene
        GetComponent<AudioSource>().PlayOneShot(pressButton);
        // currentGameSituation.activeScene = 0;
        // gameMenu.SetActive(true);
        // finishEndlessMenu.SetActive(false);

        currentGameSituation.inGame = false;

        SceneManager.LoadScene(0);
        Time.timeScale = 1; // game unpaused
    }
    // public void RepeatEndless() { // repeat scene
    //     endlessScore.Score = 0; // if its endless mode
    //     currentGameSituation.gameOver = false;

    //     gameMenu.SetActive(true);
    //     finishEndlessMenu.SetActive(false);
    //     Time.timeScale = 1; // game unpaused
    //     SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    // }

    // public void Repeat() { // repeat scene LevelIfLost
    //     //Debug.Log(activeScene);
    //     if(currentGameSituation.activeScene != 1) {
    //         CakieController.countLoon = 0; // if its story mode
    //         levelChunksGenerator.countLong = 0; // if its story mode
            
    //         currentGameSituation.finish = false; // if its story mode
    //         currentGameSituation.allChunksSpawned = false; // if its story mode
            
    //         currentGameSituation.endlessMode = false;
    //     } else {
    //         endlessScore.Score = 0; // if its endless mode
    //     }
    //     boxoonController.idleSpeed = 1;
    //     boxoonController.moveSpeed = 3;
        
    //     currentGameSituation.gameOver = false;
    //     //Debug.Log(currentGameSituation.gameOver);
         
    //     SceneManager.LoadScene(currentGameSituation.activeScene); // SceneManager.GetActiveScene().buildIndex
    //     Time.timeScale = 1; // game unpaused

    //     endlessScore.Score = 0; // if its endless mode

    //     //currentGameSituation.transition = false;
    // }
    
    // public void RepeatLevelIfFinished() { // repeat scene
    //     CakieController.countLoon = 0; // if its story mode
    //     levelChunksGenerator.countLong = 0; // if its story mode
    //     endlessScore.Score = 0; // if its endless mode
    //     currentGameSituation.finish = false; // if its story mode
    //     currentGameSituation.gameOver = false;
    //     currentGameSituation.allChunksSpawned = false; // if its story mode

    //     Time.timeScale = 1; // game unpaused
    //     SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    // }
    public void unPause() { // to game from pause menu
        GetComponent<AudioSource>().PlayOneShot(pressButton);
        currentGameSituation.transition = false; // game paused

        miniMenu.SetActive(false);
        gameMenu.SetActive(true);
        Time.timeScale = 1; // game unpaused
    }
    public void finishEndless() { // game over menu (endless mode)
        currentGameSituation.transition = true; // game finished
        
        countGameEnd++;
        showGameEndAd();

        gameMenu.SetActive(false);
        finishEndlessMenu.SetActive(true);
        
        if(currentGameSituation.Score > scoreUpdate.bestScore) { scoreUpdate.bestScore = currentGameSituation.Score; } // update best score

        PlayerPrefs.SetInt("valueBurstLoons", scoreUpdate.burstLoons);
        PlayerPrefs.SetFloat("valueBestScore", scoreUpdate.bestScore);
    }
    public void GameOver() { // game over menu (story mode)
        currentGameSituation.transition = true; // game finished
        
        countGameEnd++;
        showGameEndAd();

        PlayerPrefs.SetInt("valueBurstLoons", scoreUpdate.burstLoons);

        gameMenu.SetActive(false);
        gameOverMenu.SetActive(true);
    }
    public void finishLevel() { // finish menu (story mode)
        currentGameSituation.transition = true; // game finished
        
        countGameEnd++;
        showGameEndAd();

        if(SceneManager.GetActiveScene().buildIndex == scoreUpdate.openedLevels) scoreUpdate.openedLevels++; // increase open levels
        
        //activeScene = SceneManager.GetActiveScene().buildIndex; // remember current scene's number

        gameMenu.SetActive(false);
        finishStoryMenu.SetActive(true);

        //Time.timeScale = 0; // game paused

        // openLevels.totalScore = 0;

        PlayerPrefs.SetInt("valueOpenedLevels", scoreUpdate.openedLevels);

        for(int loonCount = 0; loonCount < CakieController.countLoon; loonCount++) { // final score = remaining loons
            finishLoons[loonCount].GetComponent<Image>().sprite = loonIconOn;
        }
    }
////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public void Settings() { // to settings from main menu
        GetComponent<AudioSource>().PlayOneShot(pressButton);
        mainMenu.SetActive(false);
        settings.SetActive(true);
    }
    public void offVolume() { // change ON button to OFF
        AudioListener.volume = 0;
        onVolumeButton.SetActive(false);
        offVolumeButton.SetActive(true);
    }
    public void onVolume() { // change OFF button to ON
        AudioListener.volume = 1;
        GetComponent<AudioSource>().PlayOneShot(pressButton);
        onVolumeButton.SetActive(true);
        offVolumeButton.SetActive(false);
    }
    public void Info() { // to info from settings
        GetComponent<AudioSource>().PlayOneShot(pressButton);
        info.SetActive(true);
        settings.SetActive(false);
    }
    public void BackFromInfo() { // to setting from info
        GetComponent<AudioSource>().PlayOneShot(pressButton);
        info.SetActive(false);
        settings.SetActive(true);
    }
    public void BackFromSettings() { // to main menu from settings
        GetComponent<AudioSource>().PlayOneShot(pressButton);
        mainMenu.SetActive(true);
        settings.SetActive(false);
    }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public void Exit() { // to exit menu from main menu
        GetComponent<AudioSource>().PlayOneShot(pressButton);
        mainMenu.SetActive(false);
        exit.SetActive(true);
    }
    public void notQuit() { // to main menu from exit menu
        GetComponent<AudioSource>().PlayOneShot(pressButton);
        mainMenu.SetActive(true);
        exit.SetActive(false);
    }
    public void Quit() { // exit from the game
        GetComponent<AudioSource>().PlayOneShot(pressButton);
        Application.Quit();
    }
///////////////////////////////////////////////////////////////////////////////
    public void OnAdLoaded(object sender, System.EventArgs args) {
        ad.Show();
    }
    public void showGameEndAd() {
        if(countGameEnd % 3 == 0) {
            ad = new InterstitialAd(gameEndAD);
            AdRequest request = new AdRequest.Builder().Build();
            ad.LoadAd(request);

            ad.OnAdLoaded += OnAdLoaded;

            // if(ad.IsLoaded())
                // ad.Show();
        }
    }
///////////////////////////////////////////////////////////////////////////////
}