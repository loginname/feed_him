﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class finishStarSound : MonoBehaviour {
    public AudioClip starSound;
    public void finishStar() {
        GetComponent<AudioSource>().PlayOneShot(starSound);
    }
}
