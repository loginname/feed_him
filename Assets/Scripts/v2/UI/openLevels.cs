﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class openLevels : MonoBehaviour {
    public static int totalScore;
    public Text totalScoreText;
    public static int levels = 1; // open levels on start
    public Sprite[] sprites = new Sprite[20];
    public Sprite starIconOff;
    public Sprite starIconOn;
    void Start() {
        totalScoreText.text = totalScore.ToString();

        for(int i = 0; i < transform.childCount; i++) {
            if(i < levels) { // when level will open set ON
                transform.GetChild(i).GetComponent<Image>().sprite = sprites[i+10];
                transform.GetChild(i).GetComponent<Button>().interactable = true;
                for(int j = 0; j < 3; j++) { // set starOFF to stars when level has opened
                    transform.GetChild(i).GetChild(j).GetComponent<Image>().sprite = starIconOff;
                }
                if(levels >= 2 && i-1 >= 0) { // set starON when level has completed or repeated
                    for(int starCount = 0; starCount < tableLevelScore.scoreS[i-1] / 5; starCount++) {
                        transform.GetChild(i-1).GetChild(starCount).GetComponent<Image>().sprite = starIconOn;
                    }
                }
            } else { // stay OFF if level hasnt opened
                transform.GetChild(i).GetComponent<Image>().sprite = sprites[i];
                transform.GetChild(i).GetComponent<Button>().interactable = false;
            }
        }
    }
}