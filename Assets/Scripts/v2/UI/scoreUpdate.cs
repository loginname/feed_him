﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scoreUpdate : MonoBehaviour {
    public static int openedLevels = 2; // value of opened levels
    public static int totalLoonsCount = 0; // total saved loons from all levels (story mode)
    public Text totalLoonsCountText; // saved loons from all levels
    public static float bestScore = 0; // best distance of all time(endless mode)
    public static int burstLoons = 0; // value of all burst loons from all levels
    public Text bestScoreText;
    public Text burstLoonsText;
    void Awake() {
        if(PlayerPrefs.HasKey("valueOpenedLevels"))
            openedLevels = PlayerPrefs.GetInt("valueOpenedLevels");
        if(PlayerPrefs.HasKey("valueBurstLoons"))
            burstLoons = PlayerPrefs.GetInt("valueBurstLoons");
        if(PlayerPrefs.HasKey("valueBestScore"))
            bestScore = PlayerPrefs.GetFloat("valueBestScore");
        if(PlayerPrefs.HasKey("valueTotalCountLoons"))
            totalLoonsCount = PlayerPrefs.GetInt("valueTotalCountLoons");
    }
    void Update() {
        bestScoreText.text = bestScore.ToString("0.0");
        burstLoonsText.text = burstLoons.ToString();
        totalLoonsCountText.text =  totalLoonsCount.ToString();
    }
}
