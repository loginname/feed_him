﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class currentGameSituation : MonoBehaviour {
    public GameObject[] setLoons = new GameObject[3]; // loons
    public static bool gameOver = false; // if game is over
    public static bool endlessMode = false; // if mode is endless
    public static bool finish = false; // if player has gone to finish (story mode)
    public static bool allChunksSpawned = false; // if all chunks have spawned
    public static bool transition = false; // pause/finish menu is active
    public static int activeScene = 0; // current number of scene
    public static bool firstLaunch = true; // if game launch first time
    public static bool inGame = false; // if player in any mode
    public GameObject Boxoon;
    public static float Score = 0; // current score
    public Text countText; // count of score in game
    public Text currentScore; // current score in this match / score after over
    public Text bestScoreFinish; // best player's score (endless mode)
    void Start() {
        for(int i = 0; i < setLoons.Length; i++) { // set loons to other objects
            loonGenerator.setLoons[i] = setLoons[i];
            boxoonController.setLoons[i] = setLoons[i];
        }

        Score = 0;

        transition = false; // game is over/paused
        gameOver = false; // game over
    }
    void Update() {
        if(activeScene == 1) { // endless mode
            countText.text = Score.ToString("0.0"); // score in game to text
            bestScoreFinish.text = scoreUpdate.bestScore.ToString("0.0"); // best score of all time (game over) to text
            currentScore.text = Score.ToString("0.0"); // score in this match (game over) to text

            if(Boxoon.transform.position.y > Score && transition == false) { // if score < boxoon position
                Score = Boxoon.transform.position.y;
            }
        }
        
        if(transition == false) {
            if(setLoons[0].CompareTag("burst") && setLoons[1].CompareTag("burst") && setLoons[2].CompareTag("burst")) { // if all loons are burst
                gameOver = true;
            }
        }
    }
    public static void Restart() {
        if(activeScene == 1) { // endless mode
            endlessMode = true;
            endlessScore.Score = 0; // score in game

            Time.timeScale = 1; // game unpaused
        } else { // story mode
            CakieController.countLoon = 0; // finish count loons
            levelChunksGenerator.countLong = 0; // how many chunk have spawned
            
            finish = false; // level finished
            allChunksSpawned = false; // limit chunks is over
            
            endlessMode = false;

            Time.timeScale = 0; // game unpaused
        }

        boxoonController.idleSpeed = 1; // set start IS
        boxoonController.moveSpeed = 3; // set start MS

        // gameOver = false;
        // transition = false;
    }
}