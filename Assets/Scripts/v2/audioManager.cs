﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class audioManager : MonoBehaviour {
    public AudioClip clickButtonSound;
    public AudioClip loonExpSound;
    public void ClickButton() {
        GetComponent<AudioSource>().PlayOneShot(clickButtonSound);
    }
    public void ExplosionOfLoon() {
        GetComponent<AudioSource>().PlayOneShot(loonExpSound);
    }
}
