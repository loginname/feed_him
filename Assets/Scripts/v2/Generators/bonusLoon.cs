﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bonusLoon : MonoBehaviour {
    private GameObject[] setLoons = new GameObject[3];
    private float startMoveSpeed; // start MS of boxoon
    private float startIdleSpeed; // start IS of boxoon
    private float changeSpeed = 4; // value edit speed of boxoon
    private int rotateSpeed = 5; // speed of rotate of bonus loon
    private int side = 1; // change the side of rotate
    public AudioClip loonInflSound;
    void Start() {
        for(int i = 0; i < 3; i++) { // set loons from loon generators
            setLoons[i] = loonGenerator.setLoons[i];
        }

        startMoveSpeed += boxoonController.moveSpeed; // set start MS of boxoon
        startIdleSpeed += boxoonController.idleSpeed; // set start IS of boxoon
    }
    void Update() {
        transform.Rotate(0, 0, Time.deltaTime * rotateSpeed * side); // idle rotate

        if(transform.eulerAngles.z <= 355 && transform.eulerAngles.z > 353) {
            side = 1;
        } else if (transform.eulerAngles.z > 5 && transform.eulerAngles.z < 7) {
            side = -1;
        }
    }
    void OnTriggerEnter2D(Collider2D loon) {
        for(int i = 0; i < 3; i++) {
            if(setLoons[i].CompareTag("burst")) {
                setLoons[i].GetComponent<AudioSource>().PlayOneShot(loonInflSound);
                setLoons[i].GetComponent<SpriteRenderer>().enabled = true;
                setLoons[i].GetComponent<CircleCollider2D>().enabled = true;
                setLoons[i].GetComponent<HingeJoint2D>().enabled = true;
                setLoons[i].tag = "loon";

                boxoonController.moveSpeed += startMoveSpeed / changeSpeed; // increase of boxoon MS
                boxoonController.idleSpeed += startIdleSpeed / changeSpeed; // increase of boxoon IS

                Destroy(gameObject); 

                break;
            }
        }
    }
}
