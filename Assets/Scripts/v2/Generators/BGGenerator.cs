﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGGenerator : MonoBehaviour {
    public Transform Camera;
    public GameObject firstBGs; // value of first backgrounds
    public backGround bgSprite; // sprite of endless bg
    public backGround firstCircleBG; // first endless bg
    private List<backGround> spawnedBG = new List<backGround>(); // list of current bgs
    void Start() {
        spawnedBG.Add(firstCircleBG);
    }
    void Update() {
        if(Camera.position.y > spawnedBG[spawnedBG.Count-1].End.position.y - 7) { // check for spawn
            SpawnBG();
        }
    }
    void SpawnBG() {
        backGround newBG = Instantiate(bgSprite);
        newBG.transform.position = spawnedBG[spawnedBG.Count-1].End.position - newBG.Begin.localPosition;
        spawnedBG.Add(newBG);

        if(spawnedBG.Count > 3) {
            Destroy(spawnedBG[0].gameObject);
            Destroy(firstBGs);
            spawnedBG.RemoveAt(0);
        }
    }
}
