﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class endlessChunksGenerator : MonoBehaviour {
    public Transform Camera;
    public Chunk[] ChunksPrefab; // all chunk can spawn
    private List<Chunk> spawnedChunks = new List<Chunk>(); // list of current spawned chunks
    void Start() {
        spawnedChunks.Add(Instantiate(ChunksPrefab[Random.Range(0, ChunksPrefab.Length)])); // add first chunk in list
    }
    void Update() {
        if(Camera.position.y > spawnedChunks[spawnedChunks.Count-1].End.position.y - 7) { // check for spawn
            SpawnChunk();
        }
    }
    void SpawnChunk() {
        Chunk newChunk = Instantiate(ChunksPrefab[Random.Range(0, ChunksPrefab.Length)]);
        newChunk.transform.position = spawnedChunks[spawnedChunks.Count-1].End.position - newChunk.Begin.localPosition;
        spawnedChunks.Add(newChunk);

        if(spawnedChunks.Count >= 3) {
            Destroy(spawnedChunks[0].gameObject);
            spawnedChunks.RemoveAt(0);
        }
    }
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    Chunk GetRandomChunk() { // check curve in chunk
        List<float> chances = new List<float>();
        for(int i = 0; i < ChunksPrefab.Length; i++) {
            chances.Add(ChunksPrefab[i].ChanceFromDistance.Evaluate(Camera.transform.position.y));
        }

        float value = Random.Range(0, chances.Sum());
        float sum = 0;

        for(int i = 0; i < chances.Count; i++) {
            sum += chances[i];
            if(value < sum) {
                return ChunksPrefab[i];
            } 
        }

        return ChunksPrefab[ChunksPrefab.Length-1];
    }
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
