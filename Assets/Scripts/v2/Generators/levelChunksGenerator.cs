﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class levelChunksGenerator : MonoBehaviour {
    public Transform Camera;
    public Chunk[] ChunksPrefab; // all chunks
    private List<Chunk> spawnedChunks = new List<Chunk>(); // current chunks
    public static Transform lastChunk; // last chunk's transform of all list
    public int howLong; // max chunks use
    public static int countLong = 0; // how much chunks used
    void Start() {
        spawnedChunks.Add(Instantiate(ChunksPrefab[Random.Range(0, ChunksPrefab.Length)])); // add first chunk in list
        countLong++; // count of used chunks ++
    }
    void Update() {
        if(Camera.position.y > spawnedChunks[spawnedChunks.Count-1].End.position.y - 7 && countLong < howLong) { // check for spawn
            SpawnChunk();
            countLong++;
        } else if(countLong == howLong) {
            lastChunk = spawnedChunks[spawnedChunks.Count-1].GetComponent<Transform>(); // get position of last chunk
            currentGameSituation.allChunksSpawned = true;
        }
    }
    void SpawnChunk() {
        Chunk newChunk = Instantiate(ChunksPrefab[Random.Range(0, ChunksPrefab.Length)]);
        newChunk.transform.position = spawnedChunks[spawnedChunks.Count-1].End.position - newChunk.Begin.localPosition;
        spawnedChunks.Add(newChunk);

        if(spawnedChunks.Count >= 3) {
            Destroy(spawnedChunks[0].gameObject);
            spawnedChunks.RemoveAt(0);
        }
    }
}
