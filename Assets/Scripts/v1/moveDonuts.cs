﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveDonuts : MonoBehaviour
{
    public float Speed;
    public Vector2 dir;
    void Update()
    {
        transform.Translate(dir*Time.deltaTime*Speed, Space.World);
        if (transform.position.y < -6)
        {
            Destroy(gameObject);
        }
    }
}
