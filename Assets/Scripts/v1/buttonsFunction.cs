﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class buttonsFunction : MonoBehaviour
{
    public GameObject mainMenu;
    public GameObject levels;
    public GameObject scoreBoard;
    public GameObject miniMenu;
    public GameObject gameMenu;
    public GameObject finishMenu;
    public GameObject settings;
    public GameObject info;
    public GameObject exit;
    public GameObject onVolumeButton;
    public GameObject offVolumeButton;
    public GameObject[] finishStars = new GameObject[3];
    public static int activeScene;
    public Sprite starIconOn;
    public Sprite starIconOff;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public void Play() {
        mainMenu.SetActive(false);
        levels.SetActive(true);
    }
    public void ScoreBoard() {
        scoreBoard.SetActive(true);
        levels.SetActive(false);
    }
    public void BackFromScoreBoard() {
        scoreBoard.SetActive(false);
        levels.SetActive(true);
    }
    public void LoadLevel(int sceneNumb) {
        SceneManager.LoadScene(sceneNumb);
        MonsterTrigger.count = 0;
        Time.timeScale = 1;
    }
    public void BackFromLevels() {
        levels.SetActive(false);
        mainMenu.SetActive(true);
    }
//////////////////////////////////////////////////////////
    public void Pause() {
        gameMenu.SetActive(false);
        miniMenu.SetActive(true);
        Time.timeScale = 0; // game paused
    }
    public void toMainMenu() {
        Time.timeScale = 1; // game unpaused
        SceneManager.LoadScene(0);
    }
    public void RepeatLevel() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        Time.timeScale = 1; // game unpaused
        MonsterTrigger.count = 0;
    }
    public void unPause() {
        miniMenu.SetActive(false);
        gameMenu.SetActive(true);
        Time.timeScale = 1; // game unpaused
    }
    public void finishLevel() {
        if(SceneManager.GetActiveScene().buildIndex == openLevels.levels) {
            openLevels.levels++;
        }
        activeScene = SceneManager.GetActiveScene().buildIndex;
        tableLevelScore.scoreUpdate();
        gameMenu.SetActive(false);
        finishMenu.SetActive(true);
        Time.timeScale = 0;
        openLevels.totalScore = 0;
        for(int i = 0; i < tableLevelScore.scoreS.Length; i++) { // + scores of all levels to TotalScore
            openLevels.totalScore += tableLevelScore.scoreS[i];
        }
        for(int starCount = 0; starCount < MonsterTrigger.count / 5; starCount++) { // Opening stars = values of score
            finishStars[starCount].GetComponent<Image>().sprite = starIconOn;
            // GameObject.Find("Star_" + starCount).GetComponent<Image>().sprite = starIconOn; /* GameObject.Find */
        }
    }
////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public void Settings() {
        mainMenu.SetActive(false);
        settings.SetActive(true);
    }
        public void offVolume() {
        onVolumeButton.SetActive(false);
        offVolumeButton.SetActive(true);
    }
    public void onVolume() {
        onVolumeButton.SetActive(true);
        offVolumeButton.SetActive(false);
    }
    public void Info() {
        info.SetActive(true);
        settings.SetActive(false);
    }
    public void BackFromInfo() {
        info.SetActive(false);
        settings.SetActive(true);
    }
    public void BackFromSettings() {
        mainMenu.SetActive(true);
        settings.SetActive(false);
    }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public void Exit() {
        mainMenu.SetActive(false);
        exit.SetActive(true);
    }
    public void notQuit() {
        mainMenu.SetActive(true);
        exit.SetActive(false);
    }
    public void Quit() {
        Application.Quit();
    }
}