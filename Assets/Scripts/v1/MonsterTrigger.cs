﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MonsterTrigger : MonoBehaviour
{
    public GameObject next;
    public GameObject[] gameStars = new GameObject[3];
    public static int count = 0;
    public Text t_count;
    public Sprite starIconOn;
    Animator _anim;

    void Start()
    {
        _anim = GetComponent<Animator>();
    }
    void Update()
    {
        t_count.text = count.ToString();
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.CompareTag("food")) {
            Destroy(other.gameObject);
            count++;
            _anim.Play("EatDonutAnim");
            _anim.SetBool("isHit", false);
        }
        if(count >=5) {
            next.SetActive(true);
        }
        for(int starCount = 0; starCount < MonsterTrigger.count / 5; starCount++) { // Opening stars = values of score
            gameStars[starCount].GetComponent<Image>().sprite = starIconOn;
            // GameObject.Find("Star_" + starCount).GetComponent<Image>().sprite = starIconOn; /* GameObject.Find */
        }
    }
}
