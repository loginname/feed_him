﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class donutGenerator : MonoBehaviour
{
    public GameObject[] DonutStuff;
	public float maxSpeed;
	public float minSpeed;
	public float maxScale;
	public float minScale;
	public float maxDelay;
	public float minDelay;
	public float minXpos;
	public float maxXpos;

	void Start () 
	{
	    StartCoroutine(CreateStar());
	}

	void Repeat () 
	{
	    StartCoroutine(CreateStar());
	}

	IEnumerator CreateStar()
	{
		yield return new WaitForSeconds(Random.Range(minDelay,maxDelay));
		Vector2 pos = new Vector2(Random.Range(minXpos,maxXpos), transform.position.y);
		float speed = Random.Range(minSpeed,maxSpeed);
		float scale = Random.Range(minScale,maxScale);
		GameObject donut1 = Instantiate(DonutStuff[Random.Range(0,DonutStuff.Length)],pos,Quaternion.identity) as GameObject;
		// StarMove sm = star.GetComponent<StarMove>();
		// sm.Speed = speed;
		donut1.transform.localScale = new Vector3(scale,scale,scale);
		Repeat();
    }
}
