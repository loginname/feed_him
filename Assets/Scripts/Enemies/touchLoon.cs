﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class touchLoon : MonoBehaviour {
    private float startMoveSpeed; // start MS of boxoon
    private float startIdleSpeed; // start IS of boxoon
    private float changeSpeed = 4; // value edit speed of boxoon
    public AudioClip loonExpSound;
    void Start() {
        startMoveSpeed += boxoonController.moveSpeed; // set start MS of boxoon
        startIdleSpeed += boxoonController.idleSpeed; // set start IS of boxoon
    }
    void OnTriggerEnter2D(Collider2D loon) {
        if (loon.CompareTag("loon")) {
            loon.GetComponent<AudioSource>().PlayOneShot(loonExpSound);
            loon.GetComponent<SpriteRenderer>().enabled = false;
            loon.GetComponent<CircleCollider2D>().enabled = false;
            loon.GetComponent<HingeJoint2D>().enabled = false;

            loon.tag = "burst";
            loonGenerator.countBurstLoons++;
            //loonGenerator.emptyLoon = true; // if any loon is burst then loon generator can generate of loon

            boxoonController.moveSpeed -= startMoveSpeed / changeSpeed; // dicrease of boxoon MS
            boxoonController.idleSpeed -= startIdleSpeed / changeSpeed; // dicrease of boxoon IS

            scoreUpdate.burstLoons++;
        }
    }
}
