﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CactusIllness : MonoBehaviour {
    public GameObject needle;
    public void spawnRoundNeedle() {
        for(int i = 0; i < 8; i++) { // circle spawn needles
            Instantiate(needle, gameObject.transform.position, Quaternion.Euler(0f, 0f, i*45f));
        }
    }
}
