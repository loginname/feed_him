﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Saw : MonoBehaviour {
    public float moveSpeed;
    ////////////////////////////////////////////////////////
    public bool isMoving = true; // if saw will be moving on straight
    public float maxX = 3;
    public float minX = -3;
    private bool changeDirection = false;
    public Vector2 moveDirection; // direction for move
    ////////////////////////////////////////////////////////
    public bool isCircle = false; // if saw will be circling
    private float startPositionY; // center
    public float radius = 1;
    public float angle = 0;
    ////////////////////////////////////////////////////////
    void Start() {
        GetComponent<AudioSource>().Play();
        startPositionY += transform.position.y;
    }
    void Update() {
        if(isMoving) { // if saw is moving on straight
            transform.Translate(moveDirection * moveSpeed * Time.deltaTime);

            if((transform.position.x <= minX && changeDirection == false) 
            || (transform.position.x >= maxX && changeDirection == false)) {
                moveDirection *= -1; // change the direction to move
                changeDirection = true;
            } else if (transform.position.x > minX 
                    && transform.position.x < maxX 
                    && changeDirection == true) {
                        changeDirection = false;
                    }
        }

        if(isCircle) { // if saw is circling
            angle += Time.deltaTime; // smooth change angle

            var x = Mathf.Cos(angle * moveSpeed) * radius;
            var y = startPositionY + Mathf.Sin(angle * moveSpeed) * radius;
            transform.position = new Vector2(x, y);
        }
    }
}

