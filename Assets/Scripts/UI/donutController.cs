﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class donutController : MonoBehaviour {
    private float Speed; // speed of current donut
    void Start() {
        Speed += donutGenerator.Speed; // set speed
    }
    void Update() {
        transform.Translate(Vector2.down * Speed * Time.deltaTime, Space.World); // move to down
        
        if(transform.position.y <= -6) { // destroy if go out from screen
            Destroy(gameObject);
        }
    }
}
