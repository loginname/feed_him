﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class donutGenerator : MonoBehaviour {
    public GameObject[] DonutStuff; // prefabs all donuts will use
	private GameObject newDonut;
	public static float Speed; // speed of donut
	private float maxSpeed = 3;
	private float minSpeed = 1;
	private float Scale; // scale of donut
	private float maxScale = 1;
	private float minScale = 0.5f;
	private float maxDelay = 1;
	private float minDelay = 0.5f;
	private Vector2 position;
	private float minXpos = -3;
	private float maxXpos = 3;
	void Start () {
	    StartCoroutine(CreateStar());
	}
	void Repeat () {
	    StartCoroutine(CreateStar());
	}
	IEnumerator CreateStar() {
		yield return new WaitForSeconds(Random.Range(minDelay, maxDelay));
		
		position = new Vector2(Random.Range(minXpos, maxXpos), transform.position.y);
		Scale = Random.Range(minScale, maxScale);
		Speed = Random.Range(minSpeed, maxSpeed);
		
		newDonut = Instantiate(DonutStuff[Random.Range(0, DonutStuff.Length)], position, Quaternion.identity) as GameObject;
		newDonut.transform.localScale = new Vector3(Scale, Scale, Scale);

		Repeat();
    }
}